﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Constants
{
    class GameConstants
    {
        public static int MinX = 0;
        public static int MaxX = 990;
        public static int MaxY = 750;
        public static int NumberOfItems = 17;
    }
}
