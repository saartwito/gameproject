﻿using FinalProject.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace FinalProject.Data
{
    class GameObject
    {
        public float X { get; set; }
        public float Y { get; set; }
        public Image ObjectImage { get; set; }
        protected int movmentSpeed { get; set; } = 5;
        public GameObject(float x, float y)
        {
            X = x;
            Y = y;
        }
       
        public virtual bool MoveObject(int direction = 0)
        {
            return false;
        }
    }
}
