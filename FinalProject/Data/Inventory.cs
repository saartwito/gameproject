﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Data
{
    class Inventory
    {
        private Item[] inventoryItems;
        private const int INVENTORY_SIZE = 100;
        public Inventory()
        {
            inventoryItems = new Item[INVENTORY_SIZE];
        }
        public int GetInventoryLength()
        {
            int amount = 0;
            for (int i = 0; i < inventoryItems.Length; i++)
            {
                if (inventoryItems[i] != null) amount++;
            }
            return amount;
        }
        public void AddItem(Item item)
        {
            for (int i = 0; i < inventoryItems.Length; i++)
            {
                if (inventoryItems[i] == null)
                {
                    for (int j = 0; j < inventoryItems.Length; j++)
                    {
                        if (inventoryItems[j] != null)
                        {
                            if (item == inventoryItems[j])
                                return;
                        }
                    }
                    inventoryItems[i] = item;
                    return;
                }
            }
        }
        public Item[] GetInventoryItems()
        {
            return inventoryItems;
        }
    }
}
