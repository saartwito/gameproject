﻿using FinalProject.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace FinalProject.Data
{
    class Item : GameObject
    {
        public bool Displayed { get; set; }
        public string Name { get; set; }

        public Item(float x, string Url, string name, float y = 0) : base(x, y)
        {
            this.Name = name;
            movmentSpeed = 3;
            Displayed = false;
            InitiateItem(Url);
        }
        private void InitiateItem(string url)
        {
            ObjectImage = new Image();
            ObjectImage.Width = 50;
            ObjectImage.Height = 50;
            ObjectImage.Source = new BitmapImage(new Uri(url));
            Canvas.SetTop(ObjectImage, Y);
            Canvas.SetLeft(ObjectImage, X);
        }
        public override bool MoveObject(int direction = 0)
        {
            if (Canvas.GetTop(ObjectImage) + movmentSpeed < GameConstants.MaxY)
            {
                Y += movmentSpeed;
                Canvas.SetTop(ObjectImage, Y);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
