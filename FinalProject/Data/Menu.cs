using FinalProject.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace FinalProject.Data
{
    class Menu
    {
        private string[] requireItems;
        private int ITEM_AMOUNT;
        private TextBlock[] itemsText;
        private Image menuImage;


        public void DisplyMenu(Canvas canvas)
        {
            menuImage = new Image();
            menuImage.Width = 1000;
            menuImage.Height = 120 * 5;
            menuImage.Source = new BitmapImage(new Uri("ms-appx:///Photos/Menu.png"));
            Canvas.SetTop(menuImage, 190);
            Canvas.SetLeft(menuImage, 780);
            canvas.Children.Add(menuImage);

        }
        public void DisplyMenuItems(Canvas canvas)
        {
            int MyY = 430;
            for (int i = 0; i < requireItems.Length; i++)
            {
                itemsText[i] = new TextBlock();
                itemsText[i].Width = 200;
                itemsText[i].Height = 100;
                itemsText[i].FontSize = 20;
                itemsText[i].Text = requireItems[i];
                Canvas.SetTop(itemsText[i], MyY);
                Canvas.SetLeft(itemsText[i], 1230);
                canvas.Children.Add(itemsText[i]);
                MyY += 50;

            }
        }
        public void ChangeMenuItemColor(string name)
        {
            for (int i = 0; i < requireItems.Length; i++)
            {
                if (itemsText[i].Text == name)
                {
                    itemsText[i].Foreground = new SolidColorBrush(Colors.Violet);
                    return;
                }
            }
        }
        public Menu(Canvas gameCanvas, int ItemsAmount = 5)
        {

            itemsText = new TextBlock[ItemsAmount];
            ITEM_AMOUNT = ItemsAmount;
            InitMenu();
            DisplyMenu(gameCanvas);
            DisplyMenuItems(gameCanvas);

        }
        public string[] GetRequiredItems()
        {
            return requireItems;
        }
        public int GetMenuLength()
        {
            return requireItems.Length;
        }
        public void InitMenu()
        {
            Random rnd = new Random();
            string[] allNames = GenerateNames();
            requireItems = new string[ITEM_AMOUNT];
            for (int i = 0; i < requireItems.Length; i++)
            {
                int num = rnd.Next(allNames.Length); // Without Bad Items
                string selectedName = allNames[num];
                if (!requireItems.Contains(selectedName))
                {
                    requireItems[i] = selectedName;
                }
                else
                {
                    i--;
                }
            }

        }
        private string[] GenerateNames()
        {
            string[] names = new string[GameConstants.NumberOfItems];
            names[0] = "Banana";
            names[1] = "Cheese";
            names[2] = "Olives";
            names[3] = "Bacon";
            names[4] = "Onion";
            names[5] = "Tomato";
            names[6] = "Mushrooms";
            names[7] = "Pepperoni";
            names[8] = "Redpepper";
            names[9] = "Spinache";
            names[10] = "Pineapple";
            return names;
        }
    }
}
