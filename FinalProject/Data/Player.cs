﻿using FinalProject.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace FinalProject.Data
{
    class Player : GameObject
    {
        private bool IsMan = true;
        private bool right = true;

        public Player(float x, float y) : base(x, y)
        {
            
            InitiateChef();
            
        }
        private void InitiateChef() //Default Chef Man
        {

            ObjectImage = new Image();
            ObjectImage.Width = 130;
            ObjectImage.Height = 130;
            ObjectImage.Source = new BitmapImage(new Uri("ms-appx:///Photos/Italian Chef.png"));
            Canvas.SetTop(ObjectImage, Y);
            Canvas.SetLeft(ObjectImage, X);
        }


        public void ChangeImage(string url)
        {
            if (url == "ms-appx:///Photos/Italian Woman Chef.png") // Woman Chef
            {
                ObjectImage.Source = new BitmapImage(new Uri(url));
                ObjectImage.Width = 110;
                ObjectImage.Height = 110;
                movmentSpeed = 5;
                IsMan = false;
            }
            else if (url == "ms-appx:///Photos/Italian Chef.png")
            {
                ObjectImage.Source = new BitmapImage(new Uri(url));
                ObjectImage.Width = 130;
                ObjectImage.Height = 130;
                movmentSpeed = 3;
                IsMan = true;
            }
            else if (url == "ms-appx:///Photos/Won.jpg")
            {
                ObjectImage.Source = new BitmapImage(new Uri(url));
                ObjectImage.Width = 500;
                ObjectImage.Height = 500;
            }
            else if (url == "ms-appx:///Photos/Lost.jpg")
            {
                ObjectImage.Source = new BitmapImage(new Uri(url));
                ObjectImage.Width = 500;
                ObjectImage.Height = 500;
            }

        }
        public override bool MoveObject(int direction = 0)
        {
            switch (direction)
            {
                case 1:

                    if (Canvas.GetLeft(ObjectImage) + movmentSpeed < GameConstants.MaxX)
                    {
                        Canvas.SetLeft(ObjectImage, Canvas.GetLeft(ObjectImage) + movmentSpeed);
                        if (IsMan)
                        {
                            if (!right)
                                ObjectImage.Source = new BitmapImage(new Uri("ms-appx:///Photos/Italian Chef.png"));

                        }
                        else if (!IsMan)
                        {
                            if (!right)
                                ObjectImage.Source = new BitmapImage(new Uri("ms-appx:///Photos/Italian woman chef left.png"));
                        }
                        right = true;
                    }
                    return true;
                case 2:
                    if (Canvas.GetLeft(ObjectImage) - movmentSpeed > GameConstants.MinX)
                        Canvas.SetLeft(ObjectImage, Canvas.GetLeft(ObjectImage) - movmentSpeed);
                    if (IsMan)
                    {
                        if (right)
                        {
                            ObjectImage.Source = new BitmapImage(new Uri("ms-appx:///Photos/Italian Chef Left.png"));
                        }
                    }
                    else if (!IsMan)
                    {
                        if (right)
                            ObjectImage.Source = new BitmapImage(new Uri("ms-appx:///Photos/Italian woman chef.png"));
                    }
                    right = false;
                    return true;
            }
            return true;
        }
    }
}