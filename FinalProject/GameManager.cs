﻿using FinalProject.Constants;
using FinalProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace FinalProject
{
    class GameManager
    {
        Random rnd;
        MediaPlayer mediaPlayer;
        DispatcherTimer DisplyItems;
        DispatcherTimer MoveItems;
        private Inventory Inventory;
        private Canvas _gameCanvas;
        private Menu _menu;
        private Player player;
        private Item[] items;
        bool GameRunning = false;
        private bool canDie;

        public GameManager(Canvas gameCanvas)
        {
            _gameCanvas = gameCanvas;
            player = new Player(100, 650);
            DisplyOnCanvas(player);
        }
        public void DisplyOnCanvas(GameObject gameObject)
        {
            try
            {
                _gameCanvas.Children.Add(gameObject.ObjectImage);
            }
            catch (Exception)
            {
            }
        }
        private void InitTimers()
        {
            // init The Items On Canvas
            DisplyItems = new DispatcherTimer();
            DisplyItems.Tick += InitItemTimer_Tick;
            DisplyItems.Interval = new TimeSpan(0, 0, 1); // Timer for the init items
            // Move The Items On Canvas
            MoveItems = new DispatcherTimer();
            MoveItems.Tick += MoveItemTimer_Tick;
            MoveItems.Interval = new TimeSpan(800);  // Timer for the items movement
        }
        private void InitItemTimer_Tick(object sender, object e)
        {
            while (true)
            {
                rnd = new Random();
                int selectedItem = rnd.Next(GameConstants.NumberOfItems);
                if (!items[selectedItem].Displayed)
                {
                    int x = rnd.Next(GameConstants.MinX, GameConstants.MaxX);
                    items[selectedItem].X = x;
                    items[selectedItem].Y = 0;
                    Canvas.SetLeft(items[selectedItem].ObjectImage, x);
                    Canvas.SetTop(items[selectedItem].ObjectImage, 0);
                    DisplyOnCanvas(items[selectedItem]);
                    items[selectedItem].Displayed = true;
                    return; 
                }
            }
        }
        private void MoveItemTimer_Tick(object sender, object e)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (items[i].Displayed)
                {
                    if (!items[i].MoveObject())
                    {
                        items[i].Displayed = false;
                        _gameCanvas.Children.Remove(items[i].ObjectImage);
                    }
                    else
                    {
                        CheckForCollisionWithPlayer(items[i]);
                    }
                }
            }
        }
        private void CheckForCollisionWithPlayer(Item item)
        {
            if (canDie)
            {
                float playerMinX = (float)Canvas.GetLeft(player.ObjectImage);
                float playerMaxX = (float)(playerMinX + player.ObjectImage.Width);
                float playerMinY = (float)Canvas.GetTop(player.ObjectImage);
                float playerMaxY = (float)(playerMinY + player.ObjectImage.Height);

                if (item.X > playerMinX && item.X < playerMaxX && item.Y + item.ObjectImage.Height - 10 > playerMinY)
                {
                    if (!CheckForLose(item))
                    {
                        item.Displayed = false;
                        _gameCanvas.Children.Remove(item.ObjectImage);
                        _menu.ChangeMenuItemColor(item.Name);
                        Inventory.AddItem(item);
                        if (Inventory.GetInventoryLength() >= _menu.GetMenuLength())
                            CheckForWin();
                    }
                }
            }
        }
        private bool CheckForLose(Item item)
        {
            bool lose = false;
            string[] wrongNames = { "Mouse", "Stinkysock" };
            for (int i = 0; i < wrongNames.Length; i++)
            {
                if (wrongNames[i] == item.Name) lose = true;
            }
            if (lose)
            {
                GameRunning = false;
                player.ChangeImage("ms-appx:///Photos/Lost.jpg");
                Canvas.SetTop(player.ObjectImage, 300);
                Canvas.SetLeft(player.ObjectImage, 100);
                DisplyItems.Stop();
                MoveItems.Stop();
                return true;
            }
            return false;
        }
        private void CheckForWin()
        {
            int counter = 0;
            string[] requiredItems = _menu.GetRequiredItems();
            for (int i = 0; i < Inventory.GetInventoryItems().Length; i++)
            {
                Item currentItem = Inventory.GetInventoryItems()[i];
                if (currentItem != null)
                    if (_menu.GetRequiredItems().Contains(currentItem.Name))
                    {
                        counter++;
                        if (counter == 7) break;
                    }
            }
            if (counter == requiredItems.Length)
            {
                canDie = false;
                DisplyItems.Stop();
                MoveItems.Stop();
                player.ChangeImage("ms-appx:///Photos/Won.jpg");
                GameRunning = false;
                Canvas.SetTop(player.ObjectImage, 300);
                Canvas.SetLeft(player.ObjectImage, 100);
                PlayerMusic();
            }
        }
        private async void PlayerMusic()
        {
            mediaPlayer = new MediaPlayer();
            StorageFolder folder = await Package.Current.InstalledLocation.GetFolderAsync(@"Media");
            StorageFile file = await folder.GetFileAsync("migos_dab.mp3");
            mediaPlayer.AutoPlay = false;
            mediaPlayer.Source = MediaSource.CreateFromStorageFile(file);
            mediaPlayer.Play();
        }
        public void InitGame()
        {
            //Create Game Data
            GameRunning = true;
            canDie = true;
            _menu = new Menu(_gameCanvas, 1);
            InitTimers();
            //Create Items
            CreateTheItems();
            DisplyItems.Start();
            MoveItems.Start();
            //Create Inventory
            Inventory = new Inventory();
        }
        private void CreateTheItems()
        {
            string[] images = GenerateImagesUrl();
            string[] names = GenerateNames();
            items = new Item[GameConstants.NumberOfItems];
            rnd = new Random();
            for (int i = 0; i < items.Length; i++)
            {
                int x = rnd.Next(GameConstants.MinX, GameConstants.MaxX);
                items[i] = new Item(x, images[i], names[i]);
            }
        }
        private string[] GenerateNames()
        {
            string[] names = new string[GameConstants.NumberOfItems];
            names[0] = "Banana";
            names[1] = "Cheese";
            names[2] = "Olives";
            names[3] = "Bacon";
            names[4] = "Onion";
            names[5] = "Tomato";
            names[6] = "Mushrooms";
            names[7] = "Pepperoni";
            names[8] = "Redpepper";
            names[9] = "Spinache";
            names[10] = "Pineapple";
            names[11] = "Stinkysock";
            names[12] = "Stinkysock";
            names[13] = "Stinkysock";
            names[14] = "Mouse";
            names[15] = "Mouse";
            names[16] = "Mouse";
            return names;
        }
        private string[] GenerateImagesUrl()
        {
            string[] images = new string[GameConstants.NumberOfItems];
            images[0] = "ms-appx:///Photos/banana.jpg";
            images[1] = "ms-appx:///Photos/cheese.jpg";
            images[2] = "ms-appx:///Photos/olive.jpg";
            images[3] = "ms-appx:///Photos/bacon.png";
            images[4] = "ms-appx:///Photos/onion.png";
            images[5] = "ms-appx:///Photos/tomato.png";
            images[6] = "ms-appx:///Photos/mushrooms (1).png";
            images[7] = "ms-appx:///Photos/pepperoni.png";
            images[8] = "ms-appx:///Photos/redpepper.png";
            images[9] = "ms-appx:///Photos/spinache.png";
            images[10] = "ms-appx:///Photos/pineapple.png";
            images[11] = "ms-appx:///Photos/stinkysock.png";
            images[12] = "ms-appx:///Photos/stinkysock.png";
            images[13] = "ms-appx:///Photos/stinkysock.png";
            images[14] = "ms-appx:///Photos/mouse.png";
            images[15] = "ms-appx:///Photos/mouse.png";
            images[16] = "ms-appx:///Photos/mouse.png";
            return images;
        }
        public void ChangeCharacterImage(string url)
        {
            player.ChangeImage(url);
        }
        public void MoveChef(int type)
        {
            if (GameRunning)
                player.MoveObject(type);
        }
    }
}
