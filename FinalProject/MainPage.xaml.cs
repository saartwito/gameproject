﻿using FinalProject.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FinalProject
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        GameManager gameManager;
        public MainPage()
        {
            this.InitializeComponent();
            gameManager = new GameManager(GameCanvas);
            CoreWindow.GetForCurrentThread().KeyDown += LorR_KeyDown;
            ManRadio.IsChecked = true;
        }
        private void LorR_KeyDown(CoreWindow sender, KeyEventArgs e)
        {
            if (e.VirtualKey == Windows.System.VirtualKey.Right)
                gameManager.MoveChef(1);
            else if (e.VirtualKey == Windows.System.VirtualKey.Left)
                gameManager.MoveChef(2);
        }
        private void ManRadio_Checked(object sender, RoutedEventArgs e)
        {
            if (ManRadio.IsChecked == true)
            {
                gameManager?.ChangeCharacterImage("ms-appx:///Photos/Italian Chef.png");
            }
        }
        private void WomanRadio_Checked(object sender, RoutedEventArgs e)
        {
            if (WomanRadio.IsChecked == true)
            {
                gameManager.ChangeCharacterImage("ms-appx:///Photos/Italian Woman Chef.png");
            }
        }
        private void PlayBtn_Click(object sender, RoutedEventArgs e)
        {
            ManRadio.Visibility = Visibility.Collapsed;
            WomanRadio.Visibility = Visibility.Collapsed;
            PlayBtn.Visibility = Visibility.Collapsed;
            gameManager.InitGame();
        }
    }
}
